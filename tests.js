test("Correct number", function (assert) {
    assert.equal(isValidPlate("1234BBB"), true, "1234 correct");
    assert.equal(isValidPlate("5678BBB"), true, "5678 correct");
    assert.equal(isValidPlate("7890BBB"), true, "90 correct");
    
});

test("correct letters", function (assert) {
    assert.equal(isValidPlate("0000bcd"), true, "lowercases");
    assert.equal(isValidPlate("0000BCD"), true, "BCD correct");
    assert.equal(isValidPlate("0000FGH"), true, "FGH correct");
    assert.equal(isValidPlate("0000JKL"), true, "JKL correct");
    assert.equal(isValidPlate("0000MNP"), true, "MNP correct");
    assert.equal(isValidPlate("0000RST"), true, "RST correct");
    assert.equal(isValidPlate("0000VWX"), true, "VWX correct");
    assert.equal(isValidPlate("0000XYZ"), true, "XYZ correct");
});

test("incorrect number", function (assert) {
    assert.equal(isValidPlate("0b00BBB"), false, "letter in number");
    assert.equal(isValidPlate("00000BBB"), false, "more than 4 digits");
    assert.equal(isValidPlate("000BBB"), false, "3 digits");
    assert.equal(isValidPlate("00BBB"), false, "2 digits");
    assert.equal(isValidPlate("0BBB"), false, "1 digits");
});

test("incorrect letters", function (assert) {
    assert.equal(isValidPlate("0000BBBB"), false, "4 letters");
    assert.equal(isValidPlate("0000BB"), false, "2 letters");
    assert.equal(isValidPlate("0000B"), false, "1 letters");
    assert.equal(isValidPlate("0000BBA"), false, "A incorrect");
    assert.equal(isValidPlate("0000BBE"), false, "E incorrect");
    assert.equal(isValidPlate("0000BBI"), false, "I incorrect");
    assert.equal(isValidPlate("0000BBO"), false, "O incorrect");
    assert.equal(isValidPlate("0000BBO"), false, "U incorrect");
    assert.equal(isValidPlate("0000BBÑ"), false, "Ñ incorrect");
    assert.equal(isValidPlate("0000BBQ"), false, "Q incorrect");
});

test("simbols", function (assert) {
    assert.equal(isValidPlate("-000BBB"), false, "- simbol number");
    assert.equal(isValidPlate("@000BBB"), false, "@ simbol number");
    assert.equal(isValidPlate("*000BBB"), false, "* simbol number");
    assert.equal(isValidPlate("0000BB+"), false, "+ simbol letters");
    assert.equal(isValidPlate("0000BB&"), false, "& simbol letters");
    assert.equal(isValidPlate("0000BB|"), false, "| simbol letters");
});
